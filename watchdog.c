#include "watchdog.h"

void Watchdog_Feed(Watchdog *watchdog)
{
	if((*watchdog) < WATCHDOG_OVERFLOW_COUNT) (*watchdog)++;
}

void Watchdog_Lose(Watchdog *watchdog)
{
	if((*watchdog) > 0) (*watchdog)--;
}

uint8_t Watchdog_IsLost(Watchdog *watchdog)
{
	return (*watchdog)==0;
}

uint8_t Watchdog_IsOverflow(Watchdog *watchdog)
{
	return (*watchdog)==WATCHDOG_OVERFLOW_COUNT;
}

void Watchdog_SetCounter(Watchdog *watchdog, uint32_t count)
{
	*watchdog = count;
}
