#ifndef __WATCHDOG_H__
#define __WATCHDOG_H__

#include <stdint.h>

#define WATCHDOG_OVERFLOW_COUNT 0xffffffff

typedef uint32_t Watchdog;

void Watchdog_Feed(Watchdog *watchdog);
void Watchdog_Lose(Watchdog *watchdog);
uint8_t Watchdog_IsLost(Watchdog *watchdog);
uint8_t Watchdog_IsOverflow(Watchdog *watchdog);
void Watchdog_SetCounter(Watchdog *watchdog, uint32_t count);

#endif 
